generate_sourcelists("./")
set(RELATIVE_PATH "")
include(auto.cmake)

get_filename_component(PWD ./ ABSOLUTE)
string(REPLACE "${LIBGEODECOMP_SOURCE_DIR}" "" RELATIVE_PATH ${PWD})
string(REPLACE "/" _ BUFFER1 ${RELATIVE_PATH})
string(REPLACE ":" _ BUFFER2 ${BUFFER1})
set(TARGET_UNIT_TEST_EXE test_${BUFFER2})
set(TARGET_RUN_UNIT_TEST run_${BUFFER2})

get_filename_component(DIRNAME ${PWD} NAME)
string(REGEX MATCH "^parallel_mpi" is_mpi_test ${DIRNAME})
string(REGEX MATCH "^unit" is_unit_test ${DIRNAME})

# check if config enabled this test class
set(allowed_test false)
if (is_unit_test)
  set(allowed_test true)
  add_custom_target(${TARGET_RUN_UNIT_TEST} echo "running tests in..." && pwd && bash -c "${UNITEXEC} ./test")
endif(is_unit_test)

if (is_mpi_test AND FEATURE_MPI)
  set(allowed_test true)
  string(REPLACE "parallel_mpi_" "" NUM_PROC ${DIRNAME})
  add_custom_target(${TARGET_RUN_UNIT_TEST} echo "running tests in..." && pwd && ${MPIEXEC} ${MPIEXEC_NUMPROC_FLAG} ${NUM_PROC} ${MPIEXEC_PREFLAGS} ./test ${MPIEXEC_POSTFLAGS})
endif()

# check whether LIMIT_TESTS selected this path, too
set(selected_test true)
if(LIMIT_TESTS)
  string(REGEX MATCH ${LIMIT_TESTS} selected_test ${RELATIVE_PATH})
endif()

if(allowed_test AND selected_test)
  set(SOURCES)
  
  foreach(header ${HEADERS})
    if(header MATCHES "test.h")
      # cxxtestgen for test headers...
      string(REPLACE "test.h" "test.cpp" source "${header}")
      list(APPEND SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/${source}")

      # make sure that each test source will include mpi.h first for
      # Intel MPI compliance (which is touchy if stdio.h has been
      # included beforehand).
      add_custom_command(
        OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/${source}"
        COMMAND "${CXX_TEST_DIR}/cxxtestgen.pl" -o "${CMAKE_CURRENT_SOURCE_DIR}/${source}.tmp" --part "${CMAKE_CURRENT_SOURCE_DIR}/${header}"
	COMMAND cat "${CMAKE_SOURCE_DIR}/fixtures/testguard.h"   >"${CMAKE_CURRENT_SOURCE_DIR}/${source}"
	COMMAND cat "${CMAKE_CURRENT_SOURCE_DIR}/${source}.tmp" >>"${CMAKE_CURRENT_SOURCE_DIR}/${source}"
	COMMAND rm -f "${CMAKE_CURRENT_SOURCE_DIR}/${source}.tmp"
        DEPENDS ${header} )
    endif()
  endforeach(header)

  # ...and main()
  list(APPEND SOURCES "${CMAKE_CURRENT_SOURCE_DIR}/main.cpp")
  add_custom_command(
    OUTPUT "${CMAKE_CURRENT_SOURCE_DIR}/main.cpp"
    COMMAND "${CXX_TEST_DIR}/cxxtestgen.pl" -o "${CMAKE_CURRENT_SOURCE_DIR}/main.cpp" --root --error-printer )

  include_directories(${LIBGEODECOMP_SOURCE_DIR}/../lib/cxxtest)
  add_executable(${TARGET_UNIT_TEST_EXE} EXCLUDE_FROM_ALL ${HEADERS} ${SOURCES})
  set_target_properties(${TARGET_UNIT_TEST_EXE} PROPERTIES OUTPUT_NAME test)
  target_link_libraries(${TARGET_UNIT_TEST_EXE} ${LOCAL_LIBGEODECOMP_LINK_LIB})

  add_dependencies(test ${TARGET_RUN_UNIT_TEST})
  add_dependencies(${TARGET_RUN_UNIT_TEST} ${TARGET_UNIT_TEST_EXE})
endif()
