#include <libgeodecomp/config.h>
#ifdef LIBGEODECOMP_FEATURE_MPI
#ifndef LIBGEODECOMP_PARALLELIZATION_HIPARSIMULATOR_GRIDVECCONV_H
#define LIBGEODECOMP_PARALLELIZATION_HIPARSIMULATOR_GRIDVECCONV_H

#include <libgeodecomp/misc/region.h>

namespace LibGeoDecomp {
namespace HiParSimulator {

class GridVecConv
{
public:
    template<typename GRID_TYPE>
    static SuperVector<typename GRID_TYPE::CellType> gridToVector(
        const GRID_TYPE& grid, 
        const Region<GRID_TYPE::DIM>& region)
    {
        SuperVector<typename GRID_TYPE::CellType> ret(region.size());
        gridToVector(grid, &ret, region);
        return ret;
    }

    template<typename GRID_TYPE>
    static void gridToVector(
        const GRID_TYPE& grid, 
        SuperVector<typename GRID_TYPE::CellType> *vec,
        const Region<GRID_TYPE::DIM>& region)
    {
        typename GRID_TYPE::CellType *dest = &(*vec)[0];

        for (typename Region<GRID_TYPE::DIM>::StreakIterator i = region.beginStreak(); 
             i != region.endStreak(); ++i) {
            const typename GRID_TYPE::CellType *start = &grid[i->origin];
            std::copy(start, start + i->length(), dest);
            dest += i->length();
        }

    }

    template<typename GRID_TYPE>
    static void vectorToGrid(
        const SuperVector<typename GRID_TYPE::CellType>& vec, 
        GRID_TYPE *grid, 
        const Region<GRID_TYPE::DIM>& region)
    {
        if (vec.size() != region.size()) {
            throw std::logic_error("region doesn't match vector size");
        }

        const typename GRID_TYPE::CellType *source = &vec[0];
        for (typename Region<GRID_TYPE::DIM>::StreakIterator i = region.beginStreak(); 
             i != region.endStreak(); ++i) {
            unsigned length = i->length();
            const typename GRID_TYPE::CellType *end = source + length;
            typename GRID_TYPE::CellType *dest = &(*grid)[i->origin];
            std::copy(source, end, dest);
            source = end;
        }
    }
};

}
}

#endif
#endif
