#ifndef LIBGEODECOMP_MISC_STRINGVEC_H
#define LIBGEODECOMP_MISC_STRINGVEC_H

#include <libgeodecomp/misc/supervector.h>

namespace LibGeoDecomp {

typedef SuperVector<std::string> StringVec;

}

#endif
